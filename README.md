The following explains the methods used to solve the assignment given with both out of box features and custom made pages.

Tools Used:
Intelli J IDE
Bit Bucket
Source Tree
Force.com Dev console

Method 1: Here out of box tools were used to make the clause, contract, and required junction relationships. Lightning was used to create the page layouts
required to view and change the data.

Method 2: Here custom pages were made from scratch using visual force. Two pages were created one for list and creating new contract and one for showing 
their related fields. One the second page one can view the realted clauses to a contract as well as add and remove them.